package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
)

func version() {
	os.Stderr.WriteString(`fuzzygrep: v0.1.0
`)
}

func usage() {
	os.Stderr.WriteString(`
Usage: fuzzygrep [OPTION] SEARCH_WORD [FILE]...
Search for SEARCH_WORD with fuzzy search in each FILE(s) or standard input.

Options:
	--help       show this help message
	--version    print the version

Report bugs to <kusabashira227@gmail.com>
`[1:])
}

func createFuzzyGrepRegexp(search string) (*regexp.Regexp, error) {
	chars := strings.Split(search, "")
	for i, ch := range chars {
		chars[i] = regexp.QuoteMeta(ch)
	}
	regSrc := strings.Join(chars, ".*")
	
	return regexp.Compile(regSrc)
}

func _main() (exitCode int, err error) {
	var (
		isHelp    = flag.Bool("help", false, "show this help message")
		isVersion = flag.Bool("version", false, "print the version")
	)
	flag.Usage = usage
	flag.Parse()

	if *isHelp {
		usage()
		return 0, nil
	}

	if *isVersion {
		version()
		return 0, nil
	}

	if flag.NArg() < 1 {
		return 1, fmt.Errorf("no specify search word")
	}

	searchWord := flag.Arg(0)
	fuzzy, err := createFuzzyGrepRegexp(searchWord)
	if err != nil {
		return 1, err
	}

	var input io.Reader
	if flag.NArg() < 2 {
		input = os.Stdin
	} else {
		inputs := make([]io.Reader, 0, 8)
		for _, fname := range flag.Args()[1:] {
			in, err := os.Open(fname)
			if err != nil {
				return 1, err
			}
			defer in.Close()
			inputs = append(inputs, in)
		}
		input = io.MultiReader(inputs...)
	}

	reader := bufio.NewScanner(input)
	for reader.Scan() {
		line := reader.Text()
		if fuzzy.MatchString(line) {
			fmt.Println(line)
		}
	}

	return 0, nil
}

func main() {
	exitCode, err := _main()
	if err != nil {
		fmt.Fprintln(os.Stderr, "fuzzygrep:", err)
	}
	os.Exit(exitCode)
}
